if (length <= 0 || characterSets == null || characterSets.isEmpty()) {
    return "";
}

return RandomStringUtils.random(
        length,
        characterSets.stream()
                .distinct()
                .map(characterSetToCharactersMap::get)
                .collect(Collectors.joining())
);
