$latex = 'latex  %O  --shell-escape %S';
$pdflatex = 'pdflatex  %O  --shell-escape %S';

$clean_ext = 'synctex.gz synctex.gz(busy) fdb_latexmk nav pytxcode snm vrb'