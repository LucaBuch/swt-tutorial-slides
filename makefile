all_pdfs = 01/swttut_01.pdf 02/swttut_02.pdf 03/swttut_03.pdf 04/swttut_04.pdf 05/swttut_05.pdf 06/swttut_06.pdf 07/swttut_07.pdf
all_texs = $(foreach pdf, $(all_pdfs),$(addsuffix .tex,$(basename $(pdf))))

all: $(all_pdfs)

ifeq ($(OS),Windows_NT)
all_blocks = $(shell dir Bloecke\ /b/s | findstr /i /e "\.tex")
all_pumls = $(shell dir Bloecke\ /b/s | findstr /r "uml\\.*\.svg")
plantuml: $(addsuffix .svg, $(basename $(shell dir .\ /b/s | findstr /i /e "\.puml")))
cleanpng:
	$(foreach file,$(all_pumls), del $(file) &&) echo
else
all_blocks = $(shell find ./Bloecke -name "*.tex")
all_pumls = $(shell find ./Bloecke/**/uml -name "*.svg")
plantuml: $(addsuffix .svg, $(basename $(shell find . -name "*.puml")))
cleanpng:
	$(foreach file,$(all_pumls), rm $(file) &&) echo
endif

static_texs = framework/PraeambelTut.tex framework/kit-colors.tex framework/gbi-macros.tex config.tex
.PHONY: all plantuml clean delete diagrams


%.svg: %.puml puml_config
	java -jar plantuml-1.2024.3.jar -tsvg -charset UTF-8 -config puml_config $<

$(all_texs): %.tex: plantuml
$(all_pdfs): %.pdf: %.tex $(static_texs) $(all_blocks)
	latexmk -shell-escape -pdflatex -cd $<

clean: cleanpng
	$(foreach tex,$(all_texs), latexmk $(tex) -cd -c &&) echo

delete: cleanpng
	$(foreach tex,$(all_texs), latexmk $(tex) -cd -C &&) echo

diagrams: plantuml