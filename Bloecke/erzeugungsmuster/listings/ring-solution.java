public class Ring<E> {
    private Producer<E> unitFactory;
    public Ring(Producer<E> unitFactory) {
        this.unitFactory = unitFactory;
    }
    public E unit() {
        return unitFactory.produce();
    }
}
