public class Abrechnungswerkzeug {
    public enum Sprache {
        DEUTSCH, ENGLISCH
    }

    private final Sprache gesetzteSprache;

    public Abrechnungswerkzeug(Sprache sprache) {
        gesetzteSprache = sprache;
    }

    public Rechnung legeRechnungAn() throws Exception {
        Rechnung rechnung;
        if (gesetzteSprache.equals(Sprache.DEUTSCH)) {
            rechnung = new RechnungDeutsch();
        } else if (gesetzteSprache.equals(Sprache.ENGLISCH)) {
            rechnung = new RechnungEnglisch();
        } else {
            throw new Exception();
        }
        // Fuege Posten hinzu, erzeuge Ausdruck, ...
        return rechnung;
    }

    public Lieferschein legeLieferscheinAn() throws Exception {
        Lieferschein lieferschein;
        if (gesetzteSprache.equals(Sprache.DEUTSCH)) {
            lieferschein = new LieferscheinDeutsch();
        } else if (gesetzteSprache.equals(Sprache.ENGLISCH)) {
            lieferschein = new LieferscheinEnglisch();
        } else {
            throw new Exception();
        }
        // Artikel hinzufuegen, fuege Adressfeld ein, ...
        return lieferschein;
    }
}