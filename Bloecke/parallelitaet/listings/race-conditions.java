//Variable in einem anderen Programmteil
int globalVar = 1;

//Operation die irgendwo im Thread aufgerufen wird
if(globalVar != 0){
    globalVar--;
}