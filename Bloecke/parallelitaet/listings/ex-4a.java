public void method1() {
    synchronized (String.class) {
        System.out.println("Acquired lock on String.class");
        synchronized (Integer.class) {
            System.out.println("Acquired lock on Integer.class");
        }
    }
}