public void insert(ListEntry n, ListEntry nextEntry) {
    ListEntry prev = nextEntry.prev;// prev != null
    prev.next = n;
    n.prev = prev;
    n.next = nextEntry;
    nextEntry.prev = n;
}