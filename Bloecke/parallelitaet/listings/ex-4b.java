public void method2() {
    synchronized (Integer.class) {
        System.out.println("Acquired lock on Integer.class");
        synchronized (String.class) {
            System.out.println("Acquired lock on String.class");
        }
    }
}