public int method(int x, double y) {
    if (x <= 0 && y <= 0) {
        throw new IllegalArgumentException("Illegal Argument");
    } else if (x == 0) {
        return (int) y;
    } else if (y == 0) {
        return x;
    } else {
        return (int) x * y;
    }
}